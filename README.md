# README #

### What is this repository for? ###

This repository is for the premium calculation based on Age, Occupation and the required cover amount.

### How do I get set up? ###

1. Once cloned, make sure npm is installed on the machine.
2. Set PremiumCalculator.Api and PremiumCalculator.UI as the start-up projects for the solution
3. Run the solution and make sure the api and ui are running.
4. Since the CORS issue is not fixed yet, open chrome using the following command
   *chrome.exe --disable-web-security --user-data-dir="c:/ChromeDevSession"*
5. Now copy the UI url (ideally https://localhost:44323/Views/Index.html) to the new chrome session.
6. The app now ready to accept data and provide the required premium amount

### Assumptions  ###

The app has been developed using ASP.NET Web API(.NET Framework 4.7.2), AngularJS on Visual Studio 2019. 
It uses Unity for Dependency Injection for the Services layer. 
It is assumed that age, coverage amount and occupation are required for calculating premium.

The occupation list is also fetched from the api. 
Also the rating is based on the occupation selected done at the api.
Ideally there should be a business layer to perform such transformation.

Below are the considerations for the logic and app development

1. For any given individual the monthly premium is calculated using the below formula

   * Death Premium = (Death Cover amount * Occupation Rating Factor * Age) /1000 * 12

2. All input fields are mandatory.

3. Given all the input fields are specified, change in the occupation dropdown should trigger the premium calculation
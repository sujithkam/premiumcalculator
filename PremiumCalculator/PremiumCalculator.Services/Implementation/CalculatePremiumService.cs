﻿using PremiumCalculator.Services.Interface;
using PremiumCalculator.Services.Models;

namespace PremiumCalculator.Services.Implementation
{
    public class CalculatePremiumService : ICalculatePremium
    {
        #region Interface Member Implementation

        public double Calculate(ServiceQueryModel query)
        {
            double result = 0;
            try
            {
                result = (query.CoverAmount * query.RatingFactor * query.Age) / 1000 * 12;
            }
            catch
            {
                //error logging
            }

            return result;
        }

        #endregion
    }
}

﻿
namespace PremiumCalculator.Services.Models
{
    public class ServiceQueryModel
    {
        public uint CoverAmount { get; set; }
        public uint Age { get; set; }        
        public double RatingFactor { get; set; }
    }
}

﻿using PremiumCalculator.Services.Models;

namespace PremiumCalculator.Services.Interface
{
    public interface ICalculatePremium
    {
        double Calculate(ServiceQueryModel query);
    }
}

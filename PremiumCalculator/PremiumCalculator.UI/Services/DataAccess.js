﻿(function () {
    'use strict';

    angular.module('premiumApp').factory('PremiumDataAccess', ['$http', '$q', function ($http, $q) {

        var apiUrl = 'https://localhost:44322/api/premium';

        var fac = {};

        fac.getOccupations = function () {
            var deferred = $q.defer();
            $http.get(apiUrl)
                .then(function successCallback(response) {
                    deferred.resolve(response.data);
                }, function errorCallback(response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;
        };

        fac.calculatePremium = function (query) {
            var deferred = $q.defer();
            $http.post(apiUrl, query)
                .then(function successCallback(response) {
                    deferred.resolve(response.data);
                }, function errorCallback(response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;
        };

        return fac;
    }]);

});
﻿(function () {
    'use strict';

    angular.module('premiumApp').factory('PremiumCalculatorSvc', ['$http', '$q', function ($http, $q) {
        var fac = {};

            fac.getOccupations = function () {
                var def = $q.defer();
                dac.getOccupations()
                    .then(function success(data) {
                        def.resolve(data);
                    }, function failure(data) {
                        def.reject(data);
                    });

                return def.promise;
            };

        fac.calculatePremium = function (query) {
            var def = $q.defer();

            dac.calculatePremium(query)
                .then(function success(data) {
                    def.resolve(data);
                }, function failure(data) {
                    def.reject(data);
                });

            return def.promise;
        };

        //data access start 
        var apiUrl = 'https://localhost:44322/api/premium';

        var dac = {};

        var headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
            'Content-Type': 'application/json'
        };


        dac.getOccupations = function () {
            var deferred = $q.defer();
            $http.get(apiUrl)
                .then(function successCallback(response) {
                    deferred.resolve(response.data);
                }, function errorCallback(response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;
        };

        dac.calculatePremium = function (query) {

            delete $http.defaults.headers.common['X-Requested-With'];

            var deferred = $q.defer();
            
            $http.post(apiUrl, query, {headers})
                .then(function successCallback(response) {
                    deferred.resolve(response.data);
                }, function errorCallback(response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;

         };

        //data access end

        return fac;
    }
    ]);

})();





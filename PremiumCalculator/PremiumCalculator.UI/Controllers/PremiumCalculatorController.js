﻿
app.controller('PremiumCalculatorController', ['$rootScope', '$scope', 'PremiumCalculatorSvc', function ($rootScope, $scope, PremiumCalculatorService) {

    $scope.OccupationList = [];
    $scope.OccupationList.push({ id: "", value: "---Select---" });

    $scope.FormData = { Name : "", Age: "", Occupation: "", CoverAmount: "" };
    $scope.DateOfBirth = "";
    $scope.CalculatedPremium = "";
    $scope.IsNameError = false;
    $scope.IsAgeError = false;
    $scope.IsCoverAmountError = false;

    $scope.load = function () {

        PremiumCalculatorService.getOccupations()
            .then(function success(data) {
                for (var i = 0; i < data.length; i++) {
                    $scope.OccupationList.push({ id: data[i].Key, value: data[i].Value });
                }
            }, function (err) {
                alert('There has been error.');
            }).catch(function (response) { alert('There has been error.'); });


        $scope.$watch('FormData.Occupation', function (v) {
            for (var i in $scope.options) {
                var option = $scope.options[i];
                if (option.name === v) {
                    $scope.FormData.Occupation = parseInt(option, 10);
                    break;
                }
            }

            if ($scope.FormData.Occupation !== "" && $scope.FormData.Occupation !== undefined) {
                $scope.calculatePremium();
            }
            else {
                $scope.CalculatedPremium = '';
            }
        });

        $scope.$watch('DateOfBirth', function (v) {
            var birthday = new Date(v);
            var today = new Date();
            var age = ((today - birthday) / (31557600000));
            $scope.FormData.Age = Math.floor(age);
        });
    };

    $scope.calculatePremium = function () {

        if ($scope.validateData()) {

            var query = JSON.stringify($scope.FormData);
            PremiumCalculatorService.calculatePremium(query)
                .then(function success(data) {
                    $scope.CalculatedPremium = data;                
                }, function (err) {
                    alert('There has been error.');
                }).catch(function (response) { alert('There has been error.'); });
        }
    };

    $scope.validateData = function () {
        var data = $scope.FormData;

        $scope.IsNameError = data.Name === '' || data.Name === undefined;
        $scope.IsAgeError = data.Age === '' || data.Age === undefined || isNaN(data.Age) ||  parseInt(data.Age, 10) < 0 
        $scope.IsCoverAmountError = data.CoverAmount === '' || data.CoverAmount === undefined || isNaN(data.CoverAmount) || parseInt(data.CoverAmount, 10) < 0;

        if (data.Occupation === "" || data.Occupation === undefined) {
            alert('Please select occupation.');
            return false;
        }

        return !$scope.IsNameError && !$scope.IsAgeError && !$scope.IsCoverAmountError;
    }

    $scope.load();

}]);
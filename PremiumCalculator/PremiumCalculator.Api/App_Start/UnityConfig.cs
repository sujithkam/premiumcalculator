using PremiumCalculator.Services.Implementation;
using PremiumCalculator.Services.Interface;
using System.Web.Http;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace PremiumCalculator.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<ICalculatePremium, CalculatePremiumService>();
            container.RegisterType<Controllers.PremiumController>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            // Configures container for WebAPI
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}
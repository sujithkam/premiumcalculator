﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace PremiumCalculator.Api.Helpers
{
    public enum Occupations
    {
        [Description("Cleaner")]
        Cleaner,
        [Description("Doctor")]
        Doctor,
        [Description("Author")]
        Author,
        [Description("Farmer")]
        Farmer,
        [Description("Mechanic")]
        Mechanic,
        [Description("Florist")]
        Florist
    }
    public enum RatingNames
    {
        [Description("Professional")]
        Professional,
        [Description("White Collar")]
        WhiteCollar,
        [Description("Light Manual")]
        LightManual,
        [Description("Heavy Manual")]
        HeavyManual
    }

    public class OccupationRating
    {
        public RatingNames Rating { get; set; }
        public double RatingFactor { get; set; }
    }

    public class Occupation
    {
        public Occupations OccupationName { get; set; }
        public OccupationRating OccupationRating { get; set; }
    }

    public static class DataHelper
    {
        private static List<OccupationRating> occupationRating = new List<OccupationRating>();

        private static List<Occupation> occupations = new List<Occupation>();

        static DataHelper()
        {
            occupationRating.Add(new OccupationRating { Rating = RatingNames.Professional, RatingFactor = 1.0 });
            occupationRating.Add(new OccupationRating { Rating = RatingNames.WhiteCollar, RatingFactor = 1.25 });
            occupationRating.Add(new OccupationRating { Rating = RatingNames.LightManual, RatingFactor = 1.50 });
            occupationRating.Add(new OccupationRating { Rating = RatingNames.HeavyManual, RatingFactor = 1.75 });

            occupations.Add(new Occupation { OccupationName = Occupations.Cleaner, OccupationRating = occupationRating.FirstOrDefault(x => x.Rating == RatingNames.LightManual) });
            occupations.Add(new Occupation { OccupationName = Occupations.Doctor, OccupationRating = occupationRating.FirstOrDefault(x => x.Rating == RatingNames.Professional) });
            occupations.Add(new Occupation { OccupationName = Occupations.Author, OccupationRating = occupationRating.FirstOrDefault(x => x.Rating == RatingNames.LightManual) });
            occupations.Add(new Occupation { OccupationName = Occupations.Farmer, OccupationRating = occupationRating.FirstOrDefault(x => x.Rating == RatingNames.HeavyManual) });
            occupations.Add(new Occupation { OccupationName = Occupations.Mechanic, OccupationRating = occupationRating.FirstOrDefault(x => x.Rating == RatingNames.HeavyManual) });
            occupations.Add(new Occupation { OccupationName = Occupations.Florist, OccupationRating = occupationRating.FirstOrDefault(x => x.Rating == RatingNames.WhiteCollar) });
        }

        public static double GetRatingFactor(int occupation)
        {
            return occupations.FirstOrDefault(x => (int)x.OccupationName == occupation).OccupationRating.RatingFactor;
        }

        public static Dictionary<int, string> GetOccupations()
        {
            return Enum.GetValues(typeof(Occupations)).Cast<Occupations>().ToDictionary(i => (int)i, t => t.ToString());
        }
    }
}
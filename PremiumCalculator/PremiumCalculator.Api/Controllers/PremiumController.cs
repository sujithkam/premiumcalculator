﻿using System.Linq;
using System.Web.Http;
using PremiumCalculator.Services.Interface;
using PremiumCalculator.Api.Models;
using PremiumCalculator.Services.Models;
using PremiumCalculator.Api.Helpers;
using System;

namespace PremiumCalculator.Api.Controllers
{
    [RoutePrefix("api/premium")]
    public class PremiumController : ApiController
    {
        #region Dependencies
        private ICalculatePremium _calculatePremiumService { get; set; }

        #endregion

        #region Constructor

        public PremiumController(ICalculatePremium calculatePremiumService) 
        {
            _calculatePremiumService = calculatePremiumService;
        }

        #endregion

        #region API Controller Methods

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetOccupationsList()
        {
            return Ok(DataHelper.GetOccupations().ToList());
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult FindPremium(ApiQueryModel data)
        {
            if(ModelState.IsValid)
            {
                double ratingFactor = DataHelper.GetRatingFactor(data.Occupation);

                var svcModel = new ServiceQueryModel
                {
                    Age = data.Age,
                    CoverAmount = data.CoverAmount,
                    RatingFactor = ratingFactor
                };

                var resultData = _calculatePremiumService.Calculate(svcModel);

                return Ok(Newtonsoft.Json.JsonConvert.SerializeObject(Math.Round(resultData, 2)));                
            }
            var errors = ModelState.SelectMany(x => x.Value.Errors)
                .Select(a => !string.IsNullOrEmpty(a.ErrorMessage) ? 
                a.ErrorMessage : a.Exception.Message).ToList();
            return BadRequest(string.Join(",", errors));
        }

        #endregion
    }
}

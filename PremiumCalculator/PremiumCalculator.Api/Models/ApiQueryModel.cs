﻿
using System.ComponentModel.DataAnnotations;

namespace PremiumCalculator.Api.Models
{
    public class ApiQueryModel
    {
        [Required]
        public uint CoverAmount { get; set; }

        [Required]
        public uint Age { get; set; }

        [Required]
        public int Occupation { get; set; }
    }
}